package ru.maxima.group1.service.impl;

import lombok.AllArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import ru.maxima.group1.dto.CredentialDto;
import ru.maxima.group1.entity.CredType;
import ru.maxima.group1.entity.Credential;
import ru.maxima.group1.mapper.CredentialMapper;
import ru.maxima.group1.repository.CredentialRepository;


@DataJpaTest
@AllArgsConstructor
class CredentialServiceImplTest {

    private final CredentialMapper credentialMapper;
    private final CredentialRepository credentialRepository;

    @Test
    void createCredential() {
        CredentialDto credentialDto=new CredentialDto();
        credentialDto.setTitle("Тест Рога и копыта");
        credentialDto.setType(CredType.UL);
        credentialDto.setInn("6449013711");
        credentialDto.setKpp("644901001");
        credentialDto.setDescription("Хорошая компания");

        Credential credential = credentialMapper.toEntity(credentialDto);
        credentialMapper.toDto(credentialRepository.save(credential));
        Assertions.assertNotNull(credential);
                //();, "Тест Рога и копыта" );
    }
}