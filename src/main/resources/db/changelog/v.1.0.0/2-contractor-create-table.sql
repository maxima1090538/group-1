--liquibase formatted sql

--changeset group1:1
create table contractor
(
    is_disable    boolean,
    created_at    timestamp(6),
    credential_id bigint
        constraint fknt58l4w50g3d5w7dy0m3ngkma
            references credential,
    id            bigserial
        primary key,
    updated_at    timestamp(6),
    confirmation  varchar(255),
    country       varchar(255) not null,
    email         varchar(255) not null,
    first_name    varchar(255) not null,
    last_name     varchar(255) not null,
    phone         varchar(255) not null,
    sur_name      varchar(255) not null
);

alter table contractor
    owner to postgres;
