--liquibase formatted sql

--changeset group1:1
create table users
(
    is_disable boolean,
    role       smallint     not null
        constraint users_role_check
            check ((role >= 0) AND (role <= 1)),
    created_at timestamp(6),
    id         bigserial
        primary key,
    last_visit timestamp(6),
    updated_at timestamp(6),
    email      varchar(255) not null,
    first_name varchar(255) not null,
    last_name  varchar(255) not null,
    login      varchar(255) not null,
    password   varchar(255) not null,
    sur_name   varchar(255) not null
);

alter table users
    owner to postgres;