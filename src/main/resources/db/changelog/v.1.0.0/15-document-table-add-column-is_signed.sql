--liquibase formatted sql

--changeset group1:1
alter table document add column is_signed boolean;
