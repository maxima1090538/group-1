--liquibase formatted sql

--changeset group1:1
create table doc_comment
(
    date_time   timestamp(6),
    document_id bigint
        constraint fk33njemr7n9hn7pr86ipssr3fd
            references document,
    id          bigserial
        primary key,
    user_id     bigint
        constraint fkojuuoo60kxjbncbpqpfdx0784
            references users,
    text        varchar(255) not null
);

alter table doc_comment
    owner to postgres;