--liquibase formatted sql

--changeset group1:1
alter table template add column updated_at timestamp(6);
