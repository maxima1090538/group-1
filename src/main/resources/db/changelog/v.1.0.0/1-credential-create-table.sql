--liquibase formatted sql

--changeset group1:1
create table credential
(
    cred_date   date,
    type        smallint     not null
        constraint credential_type_check
            check ((type >= 0) AND (type <= 1)),
    id          bigserial
        primary key,
    description varchar(255) not null,
    inn         varchar(255) not null,
    kpp         varchar(255) not null,
    title       varchar(255) not null
);

alter table credential
    owner to postgres;