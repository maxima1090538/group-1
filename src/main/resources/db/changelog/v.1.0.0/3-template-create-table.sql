--liquibase formatted sql

--changeset group1:1
create table template
(
    created_at timestamp(6),
    date_end   timestamp(6),
    date_start timestamp(6),
    doc_count  bigint,
    id         bigserial
        primary key,
    file       varchar(255),
    name       varchar(255) not null,
    type_doc   varchar(255) not null,
    version    varchar(255) not null
);

alter table template
    owner to postgres;