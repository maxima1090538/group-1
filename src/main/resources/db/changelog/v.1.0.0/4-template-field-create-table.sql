--liquibase formatted sql

--changeset group1:1
create table template_field
(
    id          bigserial
        primary key,
    template_id bigint
        constraint fk501e0dnf42psi5q8iemmui1yk
            references template,
    description varchar(255),
    placeholder varchar(255),
    name varchar(255) not null
);

alter table template_field
    owner to postgres;
