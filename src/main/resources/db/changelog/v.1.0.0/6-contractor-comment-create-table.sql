--liquibase formatted sql

--changeset group1:1
create table contactor_comment
(
    contractor_id bigint
        constraint fkk3f4y3w45romoyxpv3qvnbph
            references contractor,
    date_time     timestamp(6),
    id            bigserial
        primary key,
    user_id       bigint
        constraint fkex5bhqyb9cjrpnqasxbfw7nge
            references users,
    text          varchar(255) not null
);

alter table contactor_comment
    owner to postgres;