--liquibase formatted sql

--changeset group1:1
create table document
(
    doc_date      date,
    contractor_id bigint
        constraint fklntajd8ddac46blq5i1n19vm5
            references contractor,
    created_at    timestamp(6),
    id            bigserial
        primary key,
    title varchar(255) not null,
    parent_doc_id bigint,
    template_id   bigint
        constraint fkeijg5cw28im526wmdis62lyi2
            references template,
    updated_at    timestamp(6),
    user_id       bigint
        constraint fkm19xjdnh3l6aueyrpm1705t52
            references users,
    version       varchar(255) not null
);

alter table document
    owner to postgres;