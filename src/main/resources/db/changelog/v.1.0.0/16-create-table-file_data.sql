--liquibase formatted sql

--changeset group1:1
create table file_data
(
    id        bigserial
        primary key,
    name      varchar(255),
    type      varchar(255),
    file_data oid
);

alter table file_data
    owner to postgres;
