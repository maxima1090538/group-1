--liquibase formatted sql

--changeset group1:1
create table doc_field
(
    doc_id bigint
        constraint fkq0xcdcxo29y1o6br71p5u3ite
            references document,
    id     bigserial
        primary key,
    title  varchar(255) not null,
    value  varchar(255)
);

alter table doc_field
    owner to postgres;