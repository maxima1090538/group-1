package ru.maxima.group1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.maxima.group1.entity.Template;

@Repository
public interface TemplateRepository extends JpaRepository <Template, Long> {
}
