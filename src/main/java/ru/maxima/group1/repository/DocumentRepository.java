
package ru.maxima.group1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.maxima.group1.entity.Document;



/**
 * @author AramaJava 03.10.2023
 */

@Repository
public interface DocumentRepository extends JpaRepository <Document, Long> {
    Document getDocumentById(Long id);
}

