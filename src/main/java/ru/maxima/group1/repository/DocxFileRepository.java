package ru.maxima.group1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.group1.entity.FileData;

import java.util.Optional;

/**
 * @author AramaJava 12.11.2023
 */

public interface DocxFileRepository extends JpaRepository<FileData, Long> {
    Optional<FileData> findById (Long id);
}
