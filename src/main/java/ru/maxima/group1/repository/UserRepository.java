package ru.maxima.group1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.maxima.group1.entity.User;


/**
 * @author AramaJava 24.09.2023
 */
@Repository
public interface UserRepository extends JpaRepository <User, Long> {
    boolean existsByLogin(String login);


    User getUserById(Long id);
}
