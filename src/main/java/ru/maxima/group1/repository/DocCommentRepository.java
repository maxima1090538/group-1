package ru.maxima.group1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.maxima.group1.entity.DocComment;

import java.util.List;

/**
 * @author AramaJava 03.10.2023
 */
@Repository
public interface DocCommentRepository extends JpaRepository <DocComment, Long> {
    List<DocComment> findByDocumentIdOrderById(Long id);
}
