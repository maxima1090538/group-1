package ru.maxima.group1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.maxima.group1.entity.Contractor;


/**
 * @author AramaJava 02.10.2023
 */
@Repository
public interface ContractorRepository extends JpaRepository <Contractor, Long> {
}
