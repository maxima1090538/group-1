package ru.maxima.group1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.maxima.group1.entity.ContractorComment;

/**
 * @author AramaJava 02.10.2023
 */
@Repository
public interface ContractorCommentRepository extends JpaRepository<ContractorComment, Long> {
}
