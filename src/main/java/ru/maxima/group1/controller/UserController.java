package ru.maxima.group1.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.maxima.group1.dto.UserDto;
import ru.maxima.group1.dto.UserResponseDto;
import ru.maxima.group1.service.UserService;
import java.util.List;

/**
 * @author AramaJava 08.10.2023
 */
@RestController
@AllArgsConstructor
@RequestMapping("api/users")
@Tag(name="Пользователи", description="Действия с пользователями")
public class UserController {

    private final UserService userService;

    // User REST API
    // создаем пользователя
    @PostMapping
    @Operation(summary = "Создание пользователя", description = "Позволяет создать пользователя")
    public ResponseEntity<UserResponseDto> createUser(@RequestBody UserDto userDto) {
        UserResponseDto savedUser = userService.createUser(userDto);
        if (savedUser == null) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
    }

    // выдать всех пользователей в системе

    @GetMapping
    @Operation(summary = "Получение всех пользователей", description = "Позволяет получить всех пользователей")
    public ResponseEntity<List<UserResponseDto>> getAllUsers() {
        List<UserResponseDto> users = userService.getAllUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    // получить пользователя по id
    @GetMapping("{id}")
    @Operation(summary = "Получение пользователя по Id", description = "Позволяет получить пользователя по Id")
    public ResponseEntity<UserResponseDto> getUserById(@PathVariable @Parameter(description = "Идентификатор пользователя") Long id) {
        UserResponseDto userResponseDto = userService.getUserById(id);
        return ResponseEntity.ok(userResponseDto);
    }

    // обновить пользователя
    @PutMapping("{id}")
    @Operation(summary = "Редактирование пользователя по Id", description = "Позволяет редактировать пользователя по Id")
    public ResponseEntity<UserResponseDto> updateUser(@PathVariable @Parameter(description = "Идентификатор пользователя") Long id,
                                                      @RequestBody UserDto updatedUser) {
        UserResponseDto userResponseDto = userService.updateUser(id, updatedUser);
        return ResponseEntity.ok(userResponseDto);
    }

    // Safe delete user
    @DeleteMapping("{id}")
    @Operation(summary = "Удаление пользователя по Id", description = "Позволяет удалить пользователя по Id")
    public ResponseEntity<UserResponseDto> deleteUser(@PathVariable @Parameter(description = "Идентификатор пользователя") Long id) {
        UserResponseDto userResponseDto = userService.deleteUser(id);
        return ResponseEntity.ok(userResponseDto);
    }

}
