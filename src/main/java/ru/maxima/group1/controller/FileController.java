package ru.maxima.group1.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.maxima.group1.service.DocxFileService;


import java.io.IOException;

/**
 * @author AramaJava 07.11.2023
 */
@RestController
@AllArgsConstructor
@RequestMapping("api/files")
@Tag(name="Файл", description="Действия с файлом")
public class FileController {


    private final DocxFileService docxFileService;

    @PostMapping()
    @Operation(summary = "Загрузка шаблона файла в бд")
    public ResponseEntity<?> uploadTemplateFile(@RequestParam("file") MultipartFile file) throws IOException {
        String uploadFile = docxFileService.uploadTemplateFile(file);
        return ResponseEntity.status(HttpStatus.OK)
                .body(uploadFile);
    }

    @GetMapping("{id}")
    @Operation(summary = "Выгрузка шаблона файла из бд")
    public ResponseEntity<?> downloadFile(@PathVariable Long id) {
        byte[] fileData = docxFileService.downloadTemplateFile(id);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment; filename=template.docx")
                .body(fileData);
    }

    @PostMapping("{id}")
    @Operation(summary = "Выгрузка шаблона файла из бд")
    public ResponseEntity<?> generateFile(@PathVariable Long id) throws Exception {
        byte[] fileData = docxFileService.generateDocxFile(id);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment; filename=out.docx")
                .body(fileData);
    }

}


