package ru.maxima.group1.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.maxima.group1.dto.DocCommentDto;
import ru.maxima.group1.service.DocCommentService;

import java.util.List;


/**
 * @author AramaJava 09.10.2023
 */


@RestController
@AllArgsConstructor
@RequestMapping("api/doc-comments")
@Tag(name="Комментарии к документам", description="Действия с комментариями к документам")
public class DocCommentController {
    private final DocCommentService docCommentService;

    @PostMapping()
    @Operation(summary = "Создание комментариев к документам", description = "Позволяет создать комментарии к документам")
    public ResponseEntity<DocCommentDto> createDocComment(@RequestBody DocCommentDto docCommentDto) {
        return new ResponseEntity<>(docCommentService.createDocComment(docCommentDto), HttpStatus.OK);
    }

    @GetMapping("document/{docId}")
    @Operation(summary = "Получение комментариев к документу по Id", description = "Позволяет получить комментарии к документу по Id")
    public ResponseEntity<List<DocCommentDto>> getDocCommentsByDocId(@PathVariable @Parameter(description = "Идентификатор комментариев") Long docId) {
        return new ResponseEntity<>(docCommentService.getDocCommentsByDocId(docId), HttpStatus.OK);
    }


}

