package ru.maxima.group1.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.maxima.group1.dto.ContractorDto;
import ru.maxima.group1.service.ContractorService;

/**
 * @author AramaJava 31.10.2023
 */
@RestController
@AllArgsConstructor
@RequestMapping("api/contractors")
@Tag(name="Контрагент", description="Действия с контрагентами")
public class ContractorController {

    private final ContractorService contractorService;
    @PostMapping()
    @Operation(summary = "Создание контрагента", description = "Позволяет создать контрагента")
    public ResponseEntity<ContractorDto> createContractor (@RequestBody ContractorDto contractorDto) {
        ContractorDto savedContractor = contractorService.createContractor(contractorDto);
        return new ResponseEntity<>(savedContractor, HttpStatus.CREATED);
    }
    @GetMapping("{id}")
    @Operation(summary = "Получение контрагента по Id", description = "Позволяет получить контрагента по Id")
    public ResponseEntity<ContractorDto> getContractorById(@PathVariable @Parameter(description = "Идентификатор контрагента") Long id) {
        ContractorDto contractorDto = contractorService.getContractorById(id);
        return ResponseEntity.ok(contractorDto);
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Удаление контрагента по Id", description = "Позволяет удалить контрагента по Id")
    public ResponseEntity<ContractorDto> deleteContractor(@PathVariable @Parameter(description = "Идентификатор контрагента") Long id) {
        return new ResponseEntity<>(contractorService.deleteContractor(id), HttpStatus.OK);
    }

    @PatchMapping("{id}")
    @Operation(summary = "Редактирование контрагента по Id", description = "Позволяет редактировать контрагента по Id")
    public ResponseEntity<ContractorDto> updateContractor(@PathVariable @Parameter(description = "Идентификатор контрагента") Long id,
                                                          @RequestBody ContractorDto contractorDto) {
        ContractorDto updateContractor = contractorService.updateContractor(id, contractorDto);
        return new ResponseEntity<>(updateContractor, HttpStatus.CREATED);
    }

}