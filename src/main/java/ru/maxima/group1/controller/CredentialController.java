package ru.maxima.group1.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.maxima.group1.dto.CredentialDto;
import ru.maxima.group1.service.CredentialService;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("api/credentials")
@Tag(name="Организация", description="Действия с организацией")
public class CredentialController {
    private final CredentialService credentialService;
    @PostMapping
    @Operation(summary = "Создание организации", description = "Позволяет создать организацию")
    public ResponseEntity<CredentialDto> createCredential(@RequestBody CredentialDto credentialDto) {
        CredentialDto savedCredential = credentialService.createCredential(credentialDto);
        return new ResponseEntity<>(savedCredential, HttpStatus.CREATED);
    }

    @GetMapping("{id}")
    @Operation(summary = "Получение организации по Id", description = "Позволяет получить организацию по Id")
    public ResponseEntity<CredentialDto> getCredentialById(@PathVariable @Parameter(description = "Идентификатор организации") Long id) {
        CredentialDto credentialDto = credentialService.getCredentialById(id);
        return ResponseEntity.ok(credentialDto);
    }

    @GetMapping
    @Operation(summary = "Получение всех организаций", description = "Позволяет получить все организации")
    public ResponseEntity<List<CredentialDto>> getAllCredential() {
        List<CredentialDto> credentialDto = credentialService.getAllCredential();
        return ResponseEntity.ok(credentialDto);
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Удаление организации по Id", description = "Позволяет удалить организацию по Id")
    public ResponseEntity<CredentialDto> deleteCredential(@PathVariable @Parameter(description = "Идентификатор организации") Long id) {
        return new ResponseEntity<>(credentialService.deleteCredential(id), HttpStatus.OK);
    }

    @PutMapping("{id}")
    @Operation(summary = "Редактирование организации по Id", description = "Позволяет редактировать организацию по Id")
    public ResponseEntity<CredentialDto> updateCredential (@PathVariable @Parameter(description = "Идентификатор организации") Long id,
                                                           @RequestBody CredentialDto credentialDto) {
        CredentialDto updateCredential = credentialService.updateCredential(id, credentialDto);
        return new ResponseEntity<>(updateCredential, HttpStatus.CREATED);
    }

}
