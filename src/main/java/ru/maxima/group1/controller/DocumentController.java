
package ru.maxima.group1.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.maxima.group1.dto.DocumentDto;
import ru.maxima.group1.service.DocumentService;
import ru.maxima.group1.service.impl.DocumentServiceImpl;

import java.util.List;


/**
 * @author AramaJava 07.10.2023
 */

@RestController
@AllArgsConstructor
@RequestMapping("api/documents")
@Tag(name = "Документ", description = "Действия с документом")
public class DocumentController {

    private final DocumentService documentService;
    private static final Logger logger = LoggerFactory.getLogger(DocumentServiceImpl.class);

    @PostMapping()
    @Operation(summary = "Создание документа", description = "Позволяет создать документ")
    public ResponseEntity<DocumentDto> createDocument(@RequestBody DocumentDto documentDto) {
        logger.info("----создание документа из шаблона---");
        return new ResponseEntity<>(documentService.create(documentDto), HttpStatus.OK);
    }

    @PostMapping("{id}")

    public ResponseEntity<DocumentDto> fillDocument(@PathVariable @Parameter(description = "Идентификатор документа") Long id, @RequestBody DocumentDto documentDto) {
        return new ResponseEntity<>(documentService.fill(id, documentDto), HttpStatus.OK);
    }


    @GetMapping("{id}")
    @Operation(summary = "Получение документа по Id", description = "Позволяет получить документ по Id")
    public ResponseEntity<DocumentDto> getDocumentById(@PathVariable @Parameter(description = "Идентификатор документа") Long id) {
        return new ResponseEntity<>(documentService.getDocumentById(id), HttpStatus.OK);
    }


    @GetMapping
    @Operation(summary = "Получение всех документов", description = "Позволяет получить все документы")
    public ResponseEntity<List<DocumentDto>> readAll() {
        return new ResponseEntity<>(documentService.getAllDocuments(), HttpStatus.OK);
    }

    @PutMapping("{id}")
    @Operation(summary = "Редактирование документа по Id", description = "Позволяет редактироваь документ по Id")
    public ResponseEntity<DocumentDto> update(@PathVariable Long id, @RequestBody DocumentDto documentDto) {
        return new ResponseEntity<>(documentService.updateDocument(id, documentDto), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Удаление документа по Id", description = "Позволяет удалить документ по Id")
    public ResponseEntity<Boolean> deleteDocument(@PathVariable @Parameter(description = "Идентификатор документа") Long id) {
        return new ResponseEntity<>(documentService.deleteDocument(id), HttpStatus.OK);
    }
}

