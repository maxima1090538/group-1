package ru.maxima.group1.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.maxima.group1.dto.TemplateDto;
import ru.maxima.group1.service.TemplateService;

import java.util.List;

/**
 * @author AramaJava 08.10.2023
 */
@RestController
@AllArgsConstructor
@RequestMapping("api/templates")
@Tag(name="Шаблон", description="Действия с шаблоном")
public class TemplateController {

    private final TemplateService templateService;


    // Template REST API
    // создаем шаблон
    @PostMapping
    @Operation(summary = "Создание шаблона документа", description = "Позволяет создать шаблон документа")
    public ResponseEntity<TemplateDto> createTemplate(@RequestBody TemplateDto templateDto) {
        TemplateDto savedTemplate = templateService.createTemplate(templateDto);
        return new ResponseEntity<>(savedTemplate, HttpStatus.CREATED);
    }

    // выдать все шаблоны
    @GetMapping
    @Operation(summary = "Получение всех шаблонов", description = "Позволяет получить все шаблоны документов")
    public ResponseEntity<List<TemplateDto>> getAllTemplates() {
        List<TemplateDto> templates = templateService.getAllTemplates();
        return ResponseEntity.ok(templates);
    }


    // получить шаблон по id
    @GetMapping("{id}")
    @Operation(summary = "Получение шаблона по Id", description = "Позволяет получить шаблон документа по Id")
    public ResponseEntity<TemplateDto> getTemplateById(@PathVariable @Parameter(description = "Идентификатор шаблона") Long id) {
        TemplateDto templateDto = templateService.getTemplateById(id);
        return ResponseEntity.ok(templateDto);
    }


    // обновить шаблон
    @PutMapping("{id}")
    @Operation(summary = "Редактирование шаблона по Id", description = "Позволяет редактировать шаблон документа по Id")
    public ResponseEntity<TemplateDto> updateTemplate (@PathVariable @Parameter(description = "Идентификатор шаблона") Long id,
                                                      @RequestBody TemplateDto templateDto) {
        TemplateDto updatedTemplate = templateService.updateTemplate(id, templateDto);
        return new ResponseEntity<>(updatedTemplate, HttpStatus.CREATED);
    }

    // Safe delete template
    @DeleteMapping("{id}")
    @Operation(summary = "Удаление шаблона по Id", description = "Позволяет удалить шаблон документа по Id")
    public ResponseEntity<TemplateDto> deleteTemplate(@PathVariable @Parameter(description = "Идентификатор шаблона") Long id) {
        return new ResponseEntity<>(templateService.deleteTemplate(id), HttpStatus.OK);
    }

}
