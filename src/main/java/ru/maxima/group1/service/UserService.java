package ru.maxima.group1.service;

import ru.maxima.group1.dto.UserDto;
import ru.maxima.group1.dto.UserResponseDto;
import java.util.List;

/**
 * @author AramaJava 24.09.2023
 */

public interface UserService {

    //create
    UserResponseDto createUser(UserDto userDto);

    //read
    UserResponseDto getUserById(Long id);

    //readAll
    List<UserResponseDto> getAllUsers();

    UserResponseDto updateUser(Long id, UserDto updatedUser);

    //delete
    UserResponseDto deleteUser(Long id);


}
