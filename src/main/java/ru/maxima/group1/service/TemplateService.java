package ru.maxima.group1.service;

import ru.maxima.group1.dto.TemplateDto;


import java.util.List;


public interface TemplateService {
    TemplateDto createTemplate(TemplateDto templateDto);

    TemplateDto getTemplateById(Long id);

    List<TemplateDto> getAllTemplates();

    TemplateDto updateTemplate(Long id, TemplateDto updatedTemplate);

    TemplateDto deleteTemplate(Long id);
}