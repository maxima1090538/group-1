package ru.maxima.group1.service;

import ru.maxima.group1.entity.ContractorComment;

import java.util.List;
import java.util.Optional;

/**
 * @author AramaJava 03.10.2023
 */

public interface ContractorCommentService {
    //create
    void saveContractorComment(ContractorComment contractorComment);

    //read
    Optional<ContractorComment> getContractorCommentById(Long id);

    //readAll
    List<ContractorComment> getAllContractorComments();

    //update
    void updateContractorComment(ContractorComment contractorComment);

    //delete
    void deleteContractorComment(ContractorComment contractorComment);
}
