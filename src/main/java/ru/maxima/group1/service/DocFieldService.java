package ru.maxima.group1.service;

import ru.maxima.group1.entity.DocField;
import java.util.List;
import java.util.Optional;

/**
 * @author AramaJava 03.10.2023
 */

public interface DocFieldService {
    void saveDocField(DocField docField);

    Optional<DocField> getDocFieldById(Long id);

    List<DocField> getAllDocField();

    void updateDocField(DocField docField);

    void deleteDocField(DocField docField);
}
