package ru.maxima.group1.service;

import ru.maxima.group1.dto.ContractorDto;

/**
 * @author AramaJava 02.10.2023
 */

public interface ContractorService {

    //create
    ContractorDto createContractor(ContractorDto contractorDto);

    //read
    ContractorDto getContractorById(Long id);

    //update
    ContractorDto updateContractor(Long id, ContractorDto contractorDto);

    //delete
    ContractorDto deleteContractor(Long id);
}
