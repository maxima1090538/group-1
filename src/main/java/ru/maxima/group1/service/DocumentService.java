package ru.maxima.group1.service;
import ru.maxima.group1.dto.DocumentDto;

import java.util.List;


/**
 * @author AramaJava 03.10.2023
 */

public interface DocumentService {
    //create

    DocumentDto create(DocumentDto documentDto);

    //fill
    DocumentDto fill(Long DocId, DocumentDto documentDto);

    //read
    DocumentDto getDocumentById(Long id);

    //readAll
    List<DocumentDto> getAllDocuments();

    //update
    DocumentDto updateDocument(Long id, DocumentDto documentDto);

    //delete
    Boolean deleteDocument(Long id);


}
