package ru.maxima.group1.service;

import ru.maxima.group1.entity.TemplateField;

import java.util.List;
import java.util.Optional;

/**
 * @author AramaJava 03.10.2023
 */

public interface TemplateFieldService {
    void saveTemplateField(TemplateField templateField);

    void saveAll(TemplateField templateField);

    Optional<TemplateField> getTemplateFieldById(Long id);

    List<TemplateField> getAllTemplateField();

    void updateTemplateField(TemplateField templateField);

    void deleteAll (TemplateField templateField);

}
