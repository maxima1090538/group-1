package ru.maxima.group1.service;


import ru.maxima.group1.dto.DocCommentDto;
import ru.maxima.group1.entity.DocComment;

import java.util.List;

/**
 * @author AramaJava 03.10.2023
 */

public interface DocCommentService {
    //create
    DocCommentDto createDocComment(DocCommentDto docCommentDto);

    //read
    DocComment getDocComment(Long id);

    //readAllbyDocId
    List<DocCommentDto> getDocCommentsByDocId(Long docId);

    //update
    void updateDocComment(DocCommentDto docCommentDto);

    //delete
    void deleteDocComment(DocCommentDto docCommentDto);


}
