package ru.maxima.group1.service.impl;


import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.group1.dto.UserDto;

import ru.maxima.group1.dto.UserResponseDto;
import ru.maxima.group1.entity.User;

import ru.maxima.group1.exception.ResourceNotFoundException;
import ru.maxima.group1.mapper.UserMapper;
import ru.maxima.group1.mapper.UserResponseMapper;
import ru.maxima.group1.repository.UserRepository;
import ru.maxima.group1.service.UserService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author AramaJava 24.09.2023
 */
@Service
@AllArgsConstructor
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final UserResponseMapper userResponseMapper;

    @Override
    public UserResponseDto createUser(UserDto userDto) {
        if (userRepository.existsByLogin(userDto.getLogin())) {
            return null;
        }
        User user = userMapper.toEntity(userDto);
        User savedUser = userRepository.save(user);
        return userResponseMapper.toDto(savedUser);
    }

    @Override
    public UserResponseDto getUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User is not exists with given id: " + id));
        return userResponseMapper.toDto(user);
    }

    @Override
    public List<UserResponseDto> getAllUsers() {
        List<User> users = userRepository.findAll();
        return users.stream().map(userResponseMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserResponseDto updateUser(Long id, UserDto updatedUser) {

        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User is not exists with given id: " + id));

        user.setLogin(updatedUser.getLogin());
        user.setFirstName(updatedUser.getFirstName());
        user.setLastName(updatedUser.getLastName());
        user.setPassword(updatedUser.getPassword());
        user.setSurName(updatedUser.getSurName());
        user.setEmail(updatedUser.getEmail());
        user.setRole(updatedUser.getRole());
        user.setIsDisable(updatedUser.getIsDisable());
        user.setUpdatedAt(LocalDateTime.now());
        User updatedUserObj = userRepository.save(user);

        return userResponseMapper.toDto(updatedUserObj);
    }

    @Override
    public UserResponseDto deleteUser(Long id) {

        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User is not exists with given id: " + id));
        user.setIsDisable(true);
        User updaterUserObj = userRepository.save(user);
        return userResponseMapper.toDto(updaterUserObj);
    }

    public boolean checkUserAlreadyExist(Long id) {
        Optional<User> user = userRepository.findById(id);
        return user.isPresent();
    }
}
