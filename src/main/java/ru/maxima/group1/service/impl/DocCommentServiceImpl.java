
package ru.maxima.group1.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.group1.dto.DocCommentDto;
import ru.maxima.group1.entity.DocComment;
import ru.maxima.group1.mapper.DocCommentMapper;
import ru.maxima.group1.repository.DocCommentRepository;
import ru.maxima.group1.repository.DocumentRepository;
import ru.maxima.group1.repository.UserRepository;
import ru.maxima.group1.service.DocCommentService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author AramaJava 03.10.2023
 */

@Service
@RequiredArgsConstructor
@Transactional
public class DocCommentServiceImpl implements DocCommentService {

    private final DocCommentRepository docCommentRepository;
    private final DocumentRepository documentRepository;
    private final DocCommentMapper docCommentMapper;
    private final UserRepository userRepository;

    @Override
    public DocCommentDto createDocComment(DocCommentDto docCommentDto) {
        DocComment saveDocComment = docCommentRepository.save(DocComment.builder()
                .text(docCommentDto.getText())
                .document(documentRepository.getDocumentById(docCommentDto.getDocumentId()))
                .author(userRepository.getUserById(docCommentDto.getAuthorId()))
                .build());
        return docCommentMapper.toDto(saveDocComment);
    }

    @Override
    public DocComment getDocComment(Long id) {
        return docCommentRepository.findById(id).orElseThrow(() -> new RuntimeException("comment not found - " + id));
    }

    @Override
    public List<DocCommentDto> getDocCommentsByDocId(Long docId) {
        List<DocComment> docCommentList = docCommentRepository.findByDocumentIdOrderById(docId);
        return docCommentList.stream().map(docCommentMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void updateDocComment(DocCommentDto docCommentDto) {
        docCommentRepository.save(docCommentMapper.toEntity(docCommentDto));
    }

    @Override
    public void deleteDocComment(DocCommentDto docCommentDto) {
        docCommentRepository.delete(docCommentMapper.toEntity(docCommentDto));
    }
}

