package ru.maxima.group1.service.impl;

import lombok.AllArgsConstructor;
import org.docx4j.model.datastorage.migration.VariablePrepare;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import ru.maxima.group1.entity.DocField;
import ru.maxima.group1.entity.Document;
import ru.maxima.group1.entity.FileData;
import ru.maxima.group1.repository.DocumentRepository;
import ru.maxima.group1.repository.DocxFileRepository;
import ru.maxima.group1.service.DocxFileService;
import ru.maxima.group1.util.FileUtils;

/**
 * @author AramaJava 07.11.2023
 */

@Service
@AllArgsConstructor
@Transactional
public class DocxFileServiceImpl implements DocxFileService {

    private final DocxFileRepository docxFileRepository;
    private final DocumentRepository documentRepository;

    public String uploadTemplateFile(MultipartFile file) throws IOException {

        FileData fileData = docxFileRepository.save(FileData.builder()
                .name(file.getOriginalFilename())
                .type(file.getContentType())
                .fileData(FileUtils.compressFile(file.getBytes())).build());

        return "file uploaded successfully : " + file.getOriginalFilename();
    }

    public byte[] downloadTemplateFile(Long id) {
        FileData dbFileData = docxFileRepository.findById(id).orElseThrow(() -> new RuntimeException("FileData not found - " + id));
        return FileUtils.decompressFile(dbFileData.getFileData());
    }

    @Override
    public byte[] generateDocxFile(Long Id) throws Exception {

        FileData dbFileData = docxFileRepository.findById(Id).orElseThrow(() -> new RuntimeException("FileData not found - " + Id));
        InputStream templateInputStream = new ByteArrayInputStream(FileUtils.decompressFile(dbFileData.getFileData()));
        Document dbDoc = documentRepository.findById(Id).orElseThrow(() -> new RuntimeException("Document not found - " + Id));
        List<DocField> docFieldList = dbDoc.getDocFieldsList();
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(templateInputStream);
        MainDocumentPart mainDocumentPart = wordMLPackage.getMainDocumentPart();

        VariablePrepare.prepare(wordMLPackage);
        HashMap<String, String> variables = new HashMap<>();

        docFieldList.forEach(df -> {
            String tagKey = df.getTitle();
            String tagValue = df.getValue();
            variables.put(tagKey, tagValue);
        });

        mainDocumentPart.variableReplace(variables);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        wordMLPackage.save(outputStream);
        return outputStream.toByteArray();
    }

}

