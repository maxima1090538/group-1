package ru.maxima.group1.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.group1.dto.CredentialDto;
import ru.maxima.group1.entity.Contractor;
import ru.maxima.group1.entity.Credential;
import ru.maxima.group1.exception.ResourceNotFoundException;
import ru.maxima.group1.mapper.CredentialMapper;
import ru.maxima.group1.repository.ContractorRepository;
import ru.maxima.group1.repository.CredentialRepository;
import ru.maxima.group1.service.CredentialService;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author AramaJava 03.10.2023
 */
@Service
@RequiredArgsConstructor
@Transactional
public class CredentialServiceImpl implements CredentialService {

    private final CredentialRepository credentialRepository;
    private final CredentialMapper credentialMapper;
    private final ContractorRepository contractorRepository;

    @Override
    public CredentialDto createCredential(CredentialDto credentialDto) {

        Credential credential = credentialMapper.toEntity(credentialDto);
        Credential credentialSave = credentialRepository.save(credential);

        List<Contractor> contractors = credentialDto.getContractors();
        List<Contractor> contractorsWithId = contractors.stream()
                .peek(c -> c.setCredential(credentialSave))
                .toList();
        contractorRepository.saveAll(contractorsWithId);

        return credentialMapper.toDto(credentialSave);
    }

    @Override
    public CredentialDto getCredentialById(Long id) {
        Credential credential = credentialRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Template is not exists with given id: " + id));
        return credentialMapper.toDto(credential);
    }

    @Override
    public List<CredentialDto> getAllCredential() {
        List<Credential> credentials = credentialRepository.findAll();
        return credentials.stream().map(credentialMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public CredentialDto updateCredential(Long id, CredentialDto credentialDto) {
        Credential credential = credentialRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Credential is not exists with given id: " + id));
        Credential updateCredential = credentialMapper.toEntity(credentialDto);
        updateCredential.setId(id);
        List<Contractor> contractors = updateCredential.getContractors();
        List<Contractor> contractorsWithId = contractors.stream()
                .peek(c -> c.setCredential(credential))
                .toList();
        contractorRepository.deleteAll(credential.getContractors());
        credentialRepository.save(updateCredential);
        credential.setContractors(contractorsWithId);
        contractorRepository.saveAll(contractorsWithId);
        return credentialMapper.toDto(updateCredential);
    }

    @Override
    public CredentialDto deleteCredential(Long id) {
        Credential credential = credentialRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Credential is not exists with given id: " + id));
        credential.setDateEnd(LocalDate.now());
        return credentialMapper.toDto(credentialRepository.save(credential));
    }


}
