package ru.maxima.group1.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.group1.entity.ContractorComment;
import ru.maxima.group1.repository.ContractorCommentRepository;
import ru.maxima.group1.service.ContractorCommentService;

import java.util.List;
import java.util.Optional;

/**
 * @author AramaJava 03.10.2023
 */
@Service
@RequiredArgsConstructor
@Transactional
public class ContractorCommentServiceImpl implements ContractorCommentService {
    private final ContractorCommentRepository contractorCommentRepository;

    @Override
    public void saveContractorComment(ContractorComment contractorComment) {
        contractorCommentRepository.save(contractorComment);
    }

    @Override
    public Optional<ContractorComment> getContractorCommentById(Long id) {
        return contractorCommentRepository.findById(id);
    }

    @Override
    public List<ContractorComment> getAllContractorComments() {
        return contractorCommentRepository.findAll();
    }

    @Override
    public void updateContractorComment(ContractorComment contractorComment) {
        contractorCommentRepository.save(contractorComment);
    }

    @Override
    public void deleteContractorComment(ContractorComment contractorComment) {
        contractorCommentRepository.delete(contractorComment);
    }
}
