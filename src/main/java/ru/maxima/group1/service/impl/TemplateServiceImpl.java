package ru.maxima.group1.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.group1.dto.TemplateDto;
import ru.maxima.group1.entity.Template;
import ru.maxima.group1.entity.TemplateField;
import ru.maxima.group1.exception.ResourceNotFoundException;
import ru.maxima.group1.mapper.TemplateMapper;
import ru.maxima.group1.repository.TemplateFieldRepository;
import ru.maxima.group1.repository.TemplateRepository;
import ru.maxima.group1.service.TemplateService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class TemplateServiceImpl implements TemplateService {

    private final TemplateRepository templateRepository;
    private final TemplateFieldRepository templateFieldRepository;
    private final TemplateMapper templateMapper;


    @Override
    public TemplateDto createTemplate(TemplateDto templateDto) {

        Template template = templateMapper.toEntity(templateDto);
        Template savedTemplate = templateRepository.save(template);

        List<TemplateField> templateFieldsList = templateDto.getTemplateFieldsList();
        List<TemplateField> templateFieldWithId = templateFieldsList.stream()
                .peek(tf -> tf.setTemplate(savedTemplate))
                .toList();

        templateFieldRepository.saveAll(templateFieldWithId);
        return templateMapper.toDto(savedTemplate);
    }

    @Override
    public TemplateDto getTemplateById(Long id) {
        Template template = templateRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Template is not exists with given id: " + id));
        return templateMapper.toDto(template);
    }

    @Override
    public List<TemplateDto> getAllTemplates() {
        List<Template> templates = templateRepository.findAll();
        return templates.stream().map(templateMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public TemplateDto updateTemplate(Long id, TemplateDto updatedTemplateDto) {

        Template template = templateRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Template is not exists with given id: " + id));

        if (template.getDocCount() == 0) {

            Template updatedTemplate = templateMapper.toEntity(updatedTemplateDto);
            updatedTemplate.setId(id);
            updatedTemplate.setTemplateFieldsList(null);
            updatedTemplate.setDocCount(0L);
            updatedTemplate.setCreatedAt(template.getCreatedAt());
            updatedTemplate.setDateStart(template.getDateStart());
            updatedTemplate.setDateEnd(template.getDateEnd());


            templateFieldRepository.deleteAll(template.getTemplateFieldsList());

            List<TemplateField> templateFieldsList = updatedTemplateDto.getTemplateFieldsList();
            List<TemplateField> templateFieldWithId = templateFieldsList.stream()
                    .peek(tf -> tf.setTemplate(template))
                    .toList();
            templateFieldRepository.saveAll(templateFieldWithId);
            updatedTemplate.setTemplateFieldsList(templateFieldWithId);
            templateRepository.save(updatedTemplate);
            return templateMapper.toDto(updatedTemplate);
        } else {
            return null;
        }

    }

    @Override
    public TemplateDto deleteTemplate(Long id) {

        Template template = templateRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Template is not exists with given id: " + id));
        template.setDateEnd(LocalDateTime.now());
        return templateMapper.toDto(templateRepository.save(template));


    }


}
