
package ru.maxima.group1.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.group1.dto.DocumentDto;
import ru.maxima.group1.entity.*;
import ru.maxima.group1.exception.ResourceNotFoundException;
import ru.maxima.group1.mapper.DocumentMapper;
import ru.maxima.group1.repository.*;
import ru.maxima.group1.service.DocumentService;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * @author AramaJava 03.10.2023
 */

@Service
@RequiredArgsConstructor
@Transactional
public class DocumentServiceImpl implements DocumentService {

    private final DocumentRepository documentRepository;
    private final DocumentMapper documentMapper;
    private final ContractorRepository contractorRepository;
    private final TemplateRepository templateRepository;
    private final DocFieldRepository docFieldRepository;

    @Override
    public DocumentDto create(DocumentDto documentDto) {

        Document document = documentMapper.toEntity(documentDto);

        Contractor contractor = contractorRepository.findById(documentDto.getContractorId()).orElseThrow(() -> new ResourceNotFoundException("Contractor is not exists with given id: " + documentDto.getContractorId()));
        document.setContractor(contractor);

        Template template = templateRepository.findById(documentDto.getTemplateId()).orElseThrow(() -> new ResourceNotFoundException("Template is not exists with given id: " + documentDto.getTemplateId()));
        document.setTemplate(template);
        Document savedDoc = documentRepository.save(document);


        List<TemplateField> templateFieldsList = template.getTemplateFieldsList();

        List<DocField> docFieldList = templateFieldsList.stream()
                .map(tf -> new DocField(null, tf.getPlaceholder(), null, savedDoc))
                .toList();

        docFieldRepository.saveAll(docFieldList);
        return documentMapper.toDto(savedDoc);
    }

    @Override
    public DocumentDto fill(Long DocId, DocumentDto documentDto) {
        Document documentFromDb = documentRepository.findById(DocId).orElseThrow(() -> new ResourceNotFoundException("Document is not exists with given id: " + DocId));
        Document documentFromDto = documentMapper.toEntity(documentDto);

        List<DocField> docFieldListFromDb = documentFromDb.getDocFieldsList();
        List<DocField> docFieldListFromDto = documentFromDto.getDocFieldsList();


/*
        IntStream.range(0, docFieldListFromDto.size())
                .forEach(i -> {
                    docFieldListFromDto.get(i).setTitle(docFieldListFromDb.get(i).getTitle());
                    docFieldListFromDto.get(i).setDocument(documentFromDb);
                });
*/


        docFieldListFromDb.forEach(docField -> {
            DocField docField1 = docFieldListFromDto.stream()
                    .filter(df2 -> Objects.equals(df2.getId(), docField.getId()))
                    .findFirst()
                    .orElse(null);

            if (docField1 != null) {
                docField.setValue(docField1.getValue());
                docField.setDocument(documentFromDb);
            }
        });

        docFieldRepository.saveAll(docFieldListFromDb);
        return documentMapper.toDto(documentFromDb);
    }

    @Transactional(readOnly = true)
    @Override
    public DocumentDto getDocumentById(Long id) {
        return documentMapper.toDto(documentRepository.findById(id).orElseThrow(() -> new RuntimeException("Document not found - " + id)));
    }

    @Override
    public List<DocumentDto> getAllDocuments() {
        List<Document> documentsList = documentRepository.findAll();
        return documentsList.stream().map(documentMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public DocumentDto updateDocument(Long id, DocumentDto documentDto) {

        Document document = documentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Document is not exists with given id: " + id));

        if (!document.getIsSigned()) {

            Document updatedDocument = documentMapper.toEntity(documentDto);

            updatedDocument.setId(id);
            updatedDocument.setCreatedAt(document.getCreatedAt());

            List<DocField> docFieldsList = documentDto.getDocFieldsList();
            docFieldRepository.deleteAll(docFieldsList);

            List<DocField> docFieldListWithId = docFieldsList.stream()
                    .peek(df -> df.setDocument(document))
                    .toList();
            docFieldRepository.saveAll(docFieldListWithId);
            updatedDocument.setDocFieldsList(docFieldListWithId);

            documentRepository.save(updatedDocument);
            return documentMapper.toDto(updatedDocument);
        } else {
            return null;
        }
    }


    @Override
    public Boolean deleteDocument(Long id) {
        Document document = documentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Document is not exists with given id: " + id));
        if (document.getIsSigned()) {
            return false;
        } else {
            documentRepository.deleteById(id);
            return true;
        }
    }

}

