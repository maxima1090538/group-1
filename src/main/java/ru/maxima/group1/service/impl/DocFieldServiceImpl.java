package ru.maxima.group1.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.group1.entity.DocField;
import ru.maxima.group1.repository.DocFieldRepository;
import ru.maxima.group1.service.DocFieldService;

import java.util.List;
import java.util.Optional;

/**
 * @author AramaJava 03.10.2023
 */
@Service
@RequiredArgsConstructor
@Transactional
public class DocFieldServiceImpl implements DocFieldService {
    private final DocFieldRepository docFieldRepository;

    @Override
    public void saveDocField(DocField docField) {
        docFieldRepository.save(docField);
    }

    @Override
    public Optional<DocField> getDocFieldById(Long id) {
        return docFieldRepository.findById(id);
    }

    @Override
    public List<DocField> getAllDocField() {
        return docFieldRepository.findAll();
    }

    @Override
    public void updateDocField(DocField docField) {
        docFieldRepository.save(docField);
    }

    @Override
    public void deleteDocField(DocField docField) {
        docFieldRepository.delete(docField);
    }
}
