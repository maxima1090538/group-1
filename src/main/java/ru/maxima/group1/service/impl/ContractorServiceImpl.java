package ru.maxima.group1.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.group1.dto.ContractorDto;
import ru.maxima.group1.entity.Contractor;
import ru.maxima.group1.entity.Credential;
import ru.maxima.group1.exception.ResourceNotFoundException;
import ru.maxima.group1.mapper.ContractorMapper;
import ru.maxima.group1.repository.ContractorRepository;
import ru.maxima.group1.repository.CredentialRepository;
import ru.maxima.group1.service.ContractorService;


/**
 * @author AramaJava 02.10.2023
 */
@Service
@RequiredArgsConstructor
@Transactional
public class ContractorServiceImpl implements ContractorService {

    private final ContractorRepository contractorRepository;
    private final ContractorMapper contractorMapper;
    private final CredentialRepository credentialRepository;

    @Override
    public ContractorDto createContractor(ContractorDto contractorDto) {
        Contractor contractor = contractorMapper.toEntity(contractorDto);
        Credential credential = credentialRepository.findById(contractorDto.getCredentialId()).orElseThrow(() -> new ResourceNotFoundException("Credential is not exists with given id: " + contractorDto.getCredentialId()));
        contractor.setCredential(credential);
        contractorRepository.save(contractor);
        return contractorMapper.toDto(contractor);
    }

    @Override
    public ContractorDto getContractorById(Long id) {
        Contractor contractor = contractorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Contractor is not exists with given id: " + id));
        return contractorMapper.toDto(contractor);
    }


    @Override
    public ContractorDto updateContractor(Long id, ContractorDto contractorDto) {
        Contractor contractor = contractorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Contractor is not exists with given id: " + id));
        Contractor updateContractor = contractorMapper.toEntity(contractorDto);

        updateContractor.setId(id);
        updateContractor.setIsDisable(contractor.getIsDisable());
        updateContractor.setCreatedAt(contractor.getCreatedAt());
        updateContractor.setCredential(contractor.getCredential());
        contractorRepository.save(updateContractor);
        return contractorMapper.toDto(updateContractor);
    }

    @Override
    public ContractorDto deleteContractor(Long id) {
        Contractor contractor = contractorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Contractor is not exists with given id: " + id));
        contractor.setIsDisable(true);
        // contractorRepository.save(contractor);  // можно явно не записывать
        return contractorMapper.toDto(contractor);
    }
}
