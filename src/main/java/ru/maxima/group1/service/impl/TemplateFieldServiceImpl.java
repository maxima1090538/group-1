package ru.maxima.group1.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.group1.entity.TemplateField;
import ru.maxima.group1.repository.TemplateFieldRepository;
import ru.maxima.group1.service.TemplateFieldService;

import java.util.List;
import java.util.Optional;

/**
 * @author AramaJava 03.10.2023
 */
@Service
@RequiredArgsConstructor
@Transactional
public class TemplateFieldServiceImpl implements TemplateFieldService {
    private final TemplateFieldRepository templateFieldRepository;

    @Override
    public void saveTemplateField(TemplateField templateField) {
        templateFieldRepository.save(templateField);
    }

    @Override
    public void saveAll(TemplateField templateField) {

    }

    @Override
    public Optional<TemplateField> getTemplateFieldById(Long id) {
        return templateFieldRepository.findById(id);
    }

    @Override
    public List<TemplateField> getAllTemplateField() {
        return templateFieldRepository.findAll();
    }

    @Override
    public void updateTemplateField(TemplateField templateField) {
        templateFieldRepository.save(templateField);
    }

    @Override
    public void deleteAll(TemplateField templateField) {

    }

}
