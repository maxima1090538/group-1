package ru.maxima.group1.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author AramaJava 06.11.2023
 */
@Service
public interface DocxFileService {
    byte[] downloadTemplateFile(Long id);

    String uploadTemplateFile(MultipartFile file) throws IOException;

    byte[] generateDocxFile(Long Id) throws Exception;

}
