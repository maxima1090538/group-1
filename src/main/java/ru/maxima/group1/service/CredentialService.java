package ru.maxima.group1.service;

import ru.maxima.group1.dto.CredentialDto;
import java.util.List;

/**
 * @author AramaJava 03.10.2023
 */

public interface CredentialService {
    //create
    CredentialDto createCredential(CredentialDto credentialDto);

    //read
    CredentialDto getCredentialById(Long id);

    //readAll
    List<CredentialDto> getAllCredential();

    //update
    CredentialDto updateCredential(Long id, CredentialDto credentialDto);

    //delete
    CredentialDto deleteCredential(Long id);
}
