package ru.maxima.group1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication (scanBasePackages = "ru.maxima.group1")
public class Group1Application {

    public static void main(String[] args) {
        SpringApplication.run(Group1Application.class, args);
    }

}
