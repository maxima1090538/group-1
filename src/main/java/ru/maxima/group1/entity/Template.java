package ru.maxima.group1.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Сущность шаблона документа")
@Entity
@Table(schema = "public", name = "template")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Template {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    private String file;
    @Column(nullable = false)
    private String typeDoc; //TODO предусмотреть справочник документов

    @Column(nullable = false)
    private String version;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime dateStart;
    private LocalDateTime dateEnd;

    @Column(name = "doc_count")
    private Long docCount;       //Счётчик документов, в которых используется данный шаблон
    @OneToMany(mappedBy = "template", fetch = FetchType.EAGER)
    private List<TemplateField> templateFieldsList;

    @PrePersist
    public void prePersist(){
        LocalDateTime currentDT = LocalDateTime.now();
        this.createdAt = currentDT;
        this.dateStart = currentDT;
        this.docCount = 0L;
    }

    @PreUpdate
    public void preUpdate() {
        //if (this.docCount == 0) {
        this.updatedAt = LocalDateTime.now();
        //}
    }
}
