package ru.maxima.group1.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author AramaJava 02.10.2023
 */
@Schema(description = "Сущность полей документа")
@Entity
@Table(schema = "public", name = "doc_field")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocField {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;
    private String value;
    @ManyToOne
    @JoinColumn(name = "doc_id", referencedColumnName = "id")
    @JsonIgnore
    private Document document;

/*
    public DocField(TemplateField templateField) {
        setTitle(templateField.getTitle());
    }
*/

}
