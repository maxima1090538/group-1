package ru.maxima.group1.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

/**
 * @author AramaJava 02.10.2023
 */
@Schema(description = "Сущность комментариев контрагента")
@Entity
@Table(schema = "public", name = "contactor_comment")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContractorComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String text;
    private LocalDateTime dateTime;
    @ManyToOne
    @JoinColumn(name = "contractor_id", referencedColumnName = "id")
    private Contractor contractor;
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User author;

    @PrePersist
    public void prePersist(){
        this.dateTime = LocalDateTime.now();
    }

}
