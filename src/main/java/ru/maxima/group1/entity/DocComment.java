package ru.maxima.group1.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

/**
 * @author AramaJava 02.10.2023
 */
@Schema(description = "Сущность комментариев к документу")
@Entity
@Table(schema = "public", name = "doc_comment")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String text;
    private LocalDateTime dateTime;

    @ManyToOne
    @JoinColumn(name = "document_id", referencedColumnName = "id")
    private Document document;
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User author;

    @PrePersist
    public void prePersist(){
        this.dateTime = LocalDateTime.now();
    }


}
