package ru.maxima.group1.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author AramaJava 02.10.2023
 */

@Schema(description = "Сущность контрагента")
@Entity
@Table(schema = "public", name = "contractor")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Contractor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Идентификатор", example = "1")
    private Long id;

    @Column(nullable = false)
    @Schema(description = "Имя", example = "Иван")
    private String firstName;

    @Schema(description = "Фамилия", example = "Иванов")
    @Column(nullable = false)
    private String lastName;

    @Schema(description = "Отчество", example = "Иванович")
    @Column(nullable = false)
    private String surName;

    @Schema(description = "Телефон")
    @Column(nullable = false)
    private String phone;

    @Schema(description = "Email", example = "ivanov@mail.ru")
    @Column(nullable = false)
    private String email;

    @Schema(description = "Страна", example = "Россия")
    @Column(nullable = false)
    private String country;

    @Schema(description = "Обоснование", example = "доверенность")
    private String confirmation;

    @Schema(description = "Заблокирован")
    private Boolean isDisable;

    @ManyToOne
    @JoinColumn(name = "credential_id", referencedColumnName = "id")
    @JsonIgnore
    private Credential credential;

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private LocalDateTime createdAt;

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private LocalDateTime updatedAt;
    @PrePersist
    public void prePersist(){
        this.createdAt = LocalDateTime.now();
        this.isDisable = false;
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
}
