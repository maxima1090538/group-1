package ru.maxima.group1.entity;

import lombok.Getter;

/**
 * @author AramaJava 03.10.2023
 */

@Getter
public enum CredType {
    FL("ФИЗЛИЦО"), UL("ЮРЛИЦО");

    private final String title;

    CredType(String title) {
        this.title = title;
    }

}
