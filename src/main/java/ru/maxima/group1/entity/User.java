package ru.maxima.group1.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


/**
 * @author AramaJava 24.09.2023
 */
@Schema(description = "Сущность пользователя")
@Entity
@Table(schema = "public", name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private Roles role;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String surName;

    @Column(nullable = false)
    private String email;
    private LocalDateTime lastVisit;

    private Boolean isDisable;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    @OneToMany(mappedBy = "author", fetch = FetchType.EAGER)
    private List<ContractorComment> contrCommentList;

    @OneToMany(mappedBy = "author", fetch = FetchType.EAGER)
    private List<DocComment> docCommentList;

    @PrePersist
    public void prePersist() {
        this.createdAt = LocalDateTime.now();
        this.isDisable = false;
    }

    @PreUpdate
    public void preUpdate() {
        this.updatedAt = LocalDateTime.now();
    }

}
