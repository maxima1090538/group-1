package ru.maxima.group1.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

/**
 * @author AramaJava 02.10.2023
 */
@Schema(description = "Сущность организации")
@Entity
@Table(schema = "public", name = "credential")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Credential {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private CredType type; //физ лицо или юр лицо

    @Column(nullable = false)
    private String title;
    private LocalDate credDate;
    private LocalDate dateEnd;

    @Column(nullable = false)
    private String inn;

    @Column(nullable = false)
    private String kpp;

    @Column(nullable = false)
    private String description;
    @OneToMany(mappedBy = "credential", fetch = FetchType.EAGER)
    private List<Contractor> contractors;


    @PrePersist
    public void prePersist(){
        this.credDate = LocalDate.now();
    }
}
