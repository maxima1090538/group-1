package ru.maxima.group1.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Сущность полей шаблона документа")
@Entity
@Table(schema = "public", name="template_field")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TemplateField {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;
    private String placeholder;
    private String description;
    @ManyToOne
    @JoinColumn(name = "template_id", referencedColumnName = "id")
    @JsonIgnore
    private Template template;
}


