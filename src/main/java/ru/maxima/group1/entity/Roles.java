package ru.maxima.group1.entity;

import lombok.Getter;
@Getter
public enum Roles {
    ADMIN, USER
}