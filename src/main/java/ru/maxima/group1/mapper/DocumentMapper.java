package ru.maxima.group1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.maxima.group1.dto.DocumentDto;
import ru.maxima.group1.entity.Document;



/**
 * @author AramaJava 07.10.2023
 */
@Mapper(componentModel = "spring", uses = Document.class)

public interface DocumentMapper {

    @Mapping(source = "contractor.id", target = "contractorId")
    @Mapping(source = "template.id", target = "templateId")
    DocumentDto toDto(Document document);


    //@Mapping(target = "id",  expression = "java(myMethod(dtoId))")

    @Mapping(target = "author", ignore = true)
    @Mapping(source = "templateId", target = "template.id")
    @Mapping(source = "contractorId", target = "contractor.id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "docFieldsList", source = "docFieldsList")
    @Mapping(target = "docCommentsList", ignore = true)
    Document toEntity(DocumentDto documentDto);

}
