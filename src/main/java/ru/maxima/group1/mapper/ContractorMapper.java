package ru.maxima.group1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.maxima.group1.dto.ContractorDto;
import ru.maxima.group1.entity.Contractor;

/**
 * @author AramaJava 07.10.2023
 */
@Mapper(componentModel = "spring")
public interface ContractorMapper {

    @Mapping(target = "credentialId", source = "credential.id")
    ContractorDto toDto(Contractor entity);


    @Mapping(target = "updatedAt",  ignore = true)
    @Mapping(target = "id",  ignore = true)
    @Mapping(target = "credential", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    Contractor toEntity(ContractorDto dto);
}
