package ru.maxima.group1.mapper;

import org.mapstruct.Mapper;
import ru.maxima.group1.dto.DocFieldDto;
import ru.maxima.group1.entity.DocField;

/**
 * @author AramaJava 07.10.2023
 */

@Mapper(componentModel = "spring")
public interface DocFieldMapper {


    DocFieldDto toDto(DocField docField);


    DocField toEntity(DocFieldDto docFieldDto);

}


