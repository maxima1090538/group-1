package ru.maxima.group1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.maxima.group1.dto.CredentialDto;
import ru.maxima.group1.entity.Credential;

/**
 * @author AramaJava 07.10.2023
 */
@Mapper(componentModel = "spring")
public interface CredentialMapper {


    CredentialDto toDto(Credential entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "dateEnd", ignore = true)
    @Mapping(target = "credDate", ignore = true)
    Credential toEntity(CredentialDto dto);
}
