package ru.maxima.group1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.maxima.group1.dto.TemplateDto;
import ru.maxima.group1.dto.TemplateFieldDto;
import ru.maxima.group1.entity.TemplateField;

/**
 * @author AramaJava 07.10.2023
 */
@Mapper(componentModel = "spring")
public interface TemplateFieldMapper {
    TemplateFieldDto toDto(TemplateField entity);

    @Mapping(target = "template", ignore = true)
    @Mapping(target = "placeholder", ignore = true)
    @Mapping(target = "description", ignore = true)
    TemplateField toEntity(TemplateDto dto);
}
