package ru.maxima.group1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.maxima.group1.dto.UserResponseDto;
import ru.maxima.group1.entity.User;

/**
 * @author AramaJava 07.10.2023
 */

@Mapper(componentModel = "spring")
public interface UserResponseMapper {
    UserResponseDto toDto(User entity);


    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "role",  ignore = true)
    @Mapping(target = "password",  ignore = true)
    @Mapping(target = "lastVisit",  ignore = true)
    @Mapping(target = "id",  ignore = true)
    @Mapping(target = "docCommentList",  ignore = true)
    @Mapping(target = "createdAt",  ignore = true)
    @Mapping(target = "contrCommentList",  ignore = true)
    User toEntity(UserResponseDto dto);
}
