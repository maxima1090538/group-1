package ru.maxima.group1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.maxima.group1.dto.UserDto;
import ru.maxima.group1.entity.User;

/**
 * @author AramaJava 07.10.2023
 */

@Mapper(componentModel = "spring")
public interface UserMapper {


    UserDto toDto(User user);

    @Mapping(target = "lastVisit",ignore = true)
    @Mapping(target = "docCommentList", ignore = true)
    @Mapping(target = "contrCommentList", ignore = true)
    User toEntity(UserDto dto);



}
