package ru.maxima.group1.mapper;

import org.mapstruct.Mapper;
import ru.maxima.group1.dto.TemplateDto;
import ru.maxima.group1.entity.Template;

/**
 * @author AramaJava 07.10.2023
 */
@Mapper(componentModel = "spring")
public interface TemplateMapper {
    TemplateDto toDto(Template template);


//    @Mapping(target = "docCount", defaultValue = "0L")
//    @Mapping(target = "docCount", ignore = true)
    Template toEntity(TemplateDto dto);
}
