package ru.maxima.group1.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.maxima.group1.dto.DocCommentDto;
import ru.maxima.group1.entity.DocComment;

/**
 * @author AramaJava 07.10.2023
 */
@Mapper(componentModel = "spring")
public interface DocCommentMapper {

    @Mapping(target = "documentId", source = "document.id")
    @Mapping(target = "authorId", source = "author.id")
    DocCommentDto toDto(DocComment entity);
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "document.id", source = "documentId")
    @Mapping(target = "author.id", source = "authorId")
    DocComment toEntity(DocCommentDto dto);
}
