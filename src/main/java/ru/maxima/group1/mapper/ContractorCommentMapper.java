package ru.maxima.group1.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.maxima.group1.dto.ContractorCommentDto;
import ru.maxima.group1.entity.ContractorComment;

@Mapper(componentModel = "spring")
public interface ContractorCommentMapper {
    ContractorCommentDto toDto(ContractorComment entity);

    @Mapping(target = "id", ignore = true)
    ContractorComment toEntity(ContractorCommentDto dto);
}
