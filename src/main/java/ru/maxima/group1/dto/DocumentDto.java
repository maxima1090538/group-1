package ru.maxima.group1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.group1.entity.DocField;

import java.time.LocalDate;
import java.util.List;


/**
 * @author AramaJava 07.10.2023
 */
@Schema(description = "Сущность документа")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentDto {
    private Long id;
    private String title;
    private Long contractorId;
    private Long parentDocId;
    private String version;
    private LocalDate docDate;
    private Long templateId;
    private Boolean isSigned;
    private List<DocField> docFieldsList;
  //  private Long authorId; ToDo добавить юзера из security
}
