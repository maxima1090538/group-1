package ru.maxima.group1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import ru.maxima.group1.entity.Roles;

import java.time.LocalDateTime;


/**
 * @author AramaJava 07.10.2023
 */
@Schema(description = "Сущность пользователя")
@Data
public class UserDto {
    private Long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String surName;
    private String email;
    private Roles role;
    private Boolean isDisable;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
