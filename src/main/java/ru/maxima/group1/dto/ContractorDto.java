package ru.maxima.group1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author AramaJava 07.10.2023
 */
@Data
@Schema(description = "Сущность контрагента")
public class ContractorDto {
    private String firstName;
    private String lastName;
    private String surName;
    private String phone;
    private String email;
    private String country;
    private String confirmation;
    private Boolean isDisable;
    private Long credentialId;
}
