package ru.maxima.group1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import ru.maxima.group1.entity.Contractor;
import ru.maxima.group1.entity.CredType;

import java.util.List;


/**
 * @author AramaJava 07.10.2023
 */

@Schema(description = "Сущность организации")
@Data
public class CredentialDto {
    private String title;
    private CredType type;
    private String inn;
    private String kpp;
    private String description;
    private List<Contractor> contractors;
}
