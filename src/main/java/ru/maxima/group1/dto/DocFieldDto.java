package ru.maxima.group1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.group1.entity.Document;

/**
 * @author AramaJava 07.10.2023
 */
@Schema(description = "Сущность полей документа")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocFieldDto {

    private Long id;
    private String title;
    private String value;
    private Document document;
}
