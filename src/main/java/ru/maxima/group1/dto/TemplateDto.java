package ru.maxima.group1.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import
 lombok.Data;
import ru.maxima.group1.entity.TemplateField;


import java.time.LocalDateTime;
import java.util.List;



/**
 * @author AramaJava 07.10.2023
 */
@Schema(description = "Сущность шаблона документа")
@Data
public class TemplateDto {
    private Long id;
    private String title;
    private String file;
    private String typeDoc;
    private String version;
    private List<TemplateField> templateFieldsList;
    private Long docCount;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime dateStart;
    private LocalDateTime dateEnd;

}
