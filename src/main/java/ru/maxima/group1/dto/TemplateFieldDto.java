package ru.maxima.group1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;


/**
 * @author AramaJava 07.10.2023
 */
@Schema(description = "Сущность полей шаблона документа")
@Data
public class TemplateFieldDto {
    private String title;
    private String placeholder; //
    private String description; // описание
}
