package ru.maxima.group1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import ru.maxima.group1.entity.Contractor;
import ru.maxima.group1.entity.User;

import java.time.LocalDateTime;

/**
 * @author AramaJava 07.10.2023
 */
@Schema(description = "Сущность комментариев контрагента")
@Data
public class ContractorCommentDto {
    private String text;
    private LocalDateTime dateTime;
    private Contractor contractor;
    private User author;
}
