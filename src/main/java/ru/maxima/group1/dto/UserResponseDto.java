package ru.maxima.group1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author AramaJava 07.10.2023
 */

@Schema(description = "Сущность пользователя без пароля")
@Data
public class UserResponseDto {
    private String login;
    private String firstName;
    private String lastName;
    private String surName;
    private String email;
    private Boolean isDisable;
}
