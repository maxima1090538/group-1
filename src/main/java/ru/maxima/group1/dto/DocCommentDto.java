package ru.maxima.group1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author AramaJava 07.10.2023
 */
@Schema(description = "Сущность комментариев к документу")
@Data
public class DocCommentDto {
    private String text;
    private LocalDateTime dateTime;
    private Long documentId;
    private Long authorId;
}
