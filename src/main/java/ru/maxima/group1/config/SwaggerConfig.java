package ru.maxima.group1.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

    /**
     * @author AramaJava 19.11.2023
     */
    @OpenAPIDefinition(
            info = @Info(
                    title = "Работа с документами",
                    description = "Приложение для работы с документами, заполнения шаблонов документов значениями из базы данных", version = "1.0.0",
                    contact = @Contact(
                            name = "Group1",
                            email = "group1@group1.ru",
                            url = "https://group1.maxima.ru"
                    )
            )
    )
    public class SwaggerConfig {

    }